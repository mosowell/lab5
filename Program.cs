﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegates
{

    delegate bool СompareMethod(Product first, Product second);
    class Product
    {
        public string name;
        public double price;
        public double count;
        public DateTime validity;

        public Product(string name, double price, double count, DateTime validity)
        {
            this.name = name;
            this.price = price;
            this.count = count;
            this.validity = validity;
        }

        public void PrintObj()
        {
            Console.WriteLine();
            Console.WriteLine($"Имя: {this.name}");
            Console.WriteLine($"Цена: {this.price}");
            Console.WriteLine($"Количество: {this.count}");
            Console.WriteLine($"Срок хранения: {this.validity}");
            Console.WriteLine();
        }

        public static bool CompareByName(Product first, Product second) 
        {
            if (string.Compare(first.name, second.name) > 0) { 
                return true;
            }
            return false;

        }

        public static bool CompareByPrice(Product first, Product second)
        {
            if (first.price > second.price)
            {
                return true;
            }
            return false;

        }

        public static bool CompareByCount(Product first, Product second)
        {
            if (first.count > second.count)
            {
                return true;
            }
            return false;

        }

        public static bool CompareByValidity(Product first, Product second)
        {
            if (first.validity > second.validity)
            {
                return true;
            }
            return false;

        }

        public static Product[] Sort(Product[] arProd, СompareMethod compareMethod)
        {
            for (int i = 0; i < arProd.Length; i++)
            {
                for (int j = i; j < arProd.Length; j++)
                {
                    if (compareMethod(arProd[i], arProd[j]))
                    {
                        Product temp = arProd[i];
                        arProd[i] = arProd[j];
                        arProd[j] = temp;
                    }
                }
            }

            return arProd;
        }

    }
    class Program
    {
        static void Error(string mess)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n" + mess + "\n");
            Console.ResetColor();
        }
        static void Main(string[] args)
        {
            Product[] arProducts = new Product[5];

            arProducts[0] = new Product("Видеокарта", 20000, 3, new DateTime(2012,01,31));
            arProducts[1] = new Product("Процессор", 55000, 1, new DateTime(2015,05,11));
            arProducts[2] = new Product("Материнская плата", 30000, 10, new DateTime(2020,07,10));
            arProducts[3] = new Product("Оперативная память", 8000, 0, new DateTime(2008,09,21));
            arProducts[4] = new Product("Охлаждение", 50000, 40, new DateTime(2012,01,31));

            while (true) 
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Выберите метод сортировки товаров:\n");
                Console.ResetColor();
                Console.WriteLine("1 - По названию");
                Console.WriteLine("2 - По цене");
                Console.WriteLine("3 - По количеству");
                Console.WriteLine("4 - По Сроку годности");
                Console.WriteLine("0 - Выйти из программы");
                Console.Write("\nВаш выбор: ");

                int key = 0;

                try
                {
                    key = int.Parse(Console.ReadLine());
                }
                catch (Exception ex)
                {
                   
                    Error($"Ошибка: {ex.Message} Повторите ввод.");
                    
                    continue;
                }

                СompareMethod сompareMethod = Product.CompareByName;

                switch (key)
                {
                    case 0:
                        Environment.Exit(0);
                        break;
                    case 1:
                        сompareMethod = Product.CompareByName;
                        break;
                    case 2:
                        сompareMethod = Product.CompareByPrice;
                        break;
                    case 3:
                        сompareMethod = Product.CompareByCount;
                        break;
                    case 4:
                        сompareMethod = Product.CompareByValidity;
                        break;

                    default:
                        Error($"'{key}' - такого варианта нет!");
                        continue;
                }

                arProducts = Product.Sort(arProducts, сompareMethod);

                foreach (Product item in arProducts)
                {
                    item.PrintObj();
                }

            }
        }
    }
}
